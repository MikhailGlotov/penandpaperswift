//
//  MainScreenViewController.swift
//  PenAndPaperSwift
//
//  Created by Mikhail Glotov on 15/11/16.
//  Copyright © 2016 Mikhail Glotov. All rights reserved.
//

import UIKit
import RealmSwift

class MainScreenViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filtersTab: UISegmentedControl!
    
    let realm = try! Realm()
    var results = try!Realm().objects(Task.self)
    let filesManager = FilesManager()
    let dateHelper = DatesManager()
    
    var selectedIndex = 0
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        switch filtersTab.selectedSegmentIndex {
        case 0:
            results = try!Realm().objects(Task.self).filter(dateHelper.getTodayDatePredicate())
            tableView.reloadData()
            break
        case 1:
            results = try!Realm().objects(Task.self).filter(dateHelper.getTomorrowDatePredicate())
            tableView.reloadData()
            break
        case 2:
            results = try!Realm().objects(Task.self).filter(dateHelper.getThisWeekDatePredicate())
            tableView.reloadData()
            break
        case 3:
            results = try!Realm().objects(Task.self).filter(dateHelper.getThisMonthDatePredicate())
            tableView.reloadData()
            break
        default:
            break
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        results = try!Realm().objects(Task.self).filter(dateHelper.getTodayDatePredicate())
        tableView.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-mm-yyyy"
        dateFormatter.dateStyle = DateFormatter.Style.long
        
        let cell:TaskTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as! TaskTableViewCell
        cell.taskTitle.text = results[indexPath.row].name
        cell.taskDate.text = dateFormatter.string(from: results[indexPath.row].date!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "ShowDetails")
        {
            if let detailedView = segue.destination as? TaskWithDetailsViewController
            {
                detailedView.taskTitle = results[selectedIndex].name
                detailedView.taskDescription = descriptionSample
                detailedView.taskId = results[selectedIndex].id
            }
        }
    }
}


