//
//  AppDelegate.swift
//  PenAndPaperSwift
//
//  Created by Mikhail Glotov on 15/11/16.
//  Copyright © 2016 Mikhail Glotov. All rights reserved.
//

import CoreData
import FBSDKCoreKit
import UserNotifications
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UITabBarDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    let notificationsManager = NotificationsManager()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UITabBar.appearance().isTranslucent = false
        UITabBar.appearance().barTintColor = purpleColor
        UITabBar.appearance().tintColor = creamColor
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {(accepted, error) in
            if !accepted {
                print("Access denied.")
            }
        }
        
        let action1 = UNNotificationAction(identifier: "remindLater", title: "Remind me later", options: [])
        let action2 = UNNotificationAction(identifier: "doneAlready", title: "Done", options: [])
        let category = UNNotificationCategory(identifier: "myCategory", actions: [action1, action2], intentIdentifiers: ["remindLater", "doneAlready"], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([category])
        UNUserNotificationCenter.current().delegate = self
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        if let _ = FBSDKAccessToken.current()
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MainScreenViewController") as UIViewController
            self.window?.rootViewController = vc
        }
        else
        {
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as UIViewController
            self.window?.rootViewController = vc
        }
        
        return true
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // Update the app interface directly.        
        // Play a sound.
        completionHandler(UNNotificationPresentationOptions.alert)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if response.actionIdentifier == "remindLater"
        {
            notificationsManager.reScheduleLocalNotification(notification: response.notification)
        }
        else if response.actionIdentifier == "doneAlready"
        {
            //remove this notification and mark task as Done
        }
    }
        
        
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        return handled
    }
}

