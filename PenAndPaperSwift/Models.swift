//
//  Task.swift
//  PenAndPaperSwift
//
//  Created by Mikhail Glotov on 15/11/16.
//  Copyright © 2016 Mikhail Glotov. All rights reserved.
//
import RealmSwift
import Realm
import Foundation

class Task: Object {
    dynamic var id = ""
    dynamic var name = ""
    dynamic var information : String?
    dynamic var date : Date?
    dynamic var taskOwner : Area?
}

class Area: Object {
    dynamic var name = ""
    dynamic var information = ""
    let listOfTasks = List<Task>()
}
