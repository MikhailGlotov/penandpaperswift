//
//  Defines.swift
//  PenAndPaperSwift
//
//  Created by Mikhail Glotov on 22/11/16.
//  Copyright © 2016 Mikhail Glotov. All rights reserved.
//

import Foundation
import UIKit

let purpleColor = UIColor(red:52/255, green:54/255, blue:89/255, alpha: 1.0)
let creamColor = UIColor(red:235/255, green:223/255, blue:223/255, alpha: 1.0)

let profileImageName = "Doggo"
let descriptionSample = "Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое. Лежа на панцирнотвердой спине, он видел, стоило ему приподнять голову, свой коричневый, выпуклый, разделенный дугообразными чешуйками живот, на верхушке которого еле держалось готовое вот-вот окончательно сползти одеяло."
let fiveMinutes = 300.0
