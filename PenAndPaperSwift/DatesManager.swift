//
//  DatesManager.swift
//  PenAndPaperSwift
//
//  Created by Mikhail Glotov on 25/11/16.
//  Copyright © 2016 Mikhail Glotov. All rights reserved.
//

import UIKit

class DatesManager: NSObject {

    func getTodayDatePredicate() -> NSPredicate {
        let todayDate = Date()
        
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        var components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: todayDate)
        
        components.hour = 00
        components.minute = 00
        components.second = 00
        let startDate = calendar.date(from: components)
        
        components.hour = 23
        components.minute = 59
        components.second = 59
        let endDate = calendar.date(from: components)
        
        return NSPredicate(format: "date >= %@ AND date =< %@", argumentArray: [startDate!, endDate!])
    }
    
    func getTomorrowDatePredicate() -> NSPredicate {
        let todayDate = Date()
        
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        var components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: todayDate)
        
        components.day! += 1
        components.hour = 00
        components.minute = 00
        components.second = 00
        let startDate = calendar.date(from: components)

        components.hour = 23
        components.minute = 59
        components.second = 59
        let endDate = calendar.date(from: components)
        
        return NSPredicate(format: "date >= %@ AND date =< %@", argumentArray: [startDate!, endDate!])
    }
    
    func getThisWeekDatePredicate() -> NSPredicate {
        let todayDate = Date()
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        var components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second, .weekdayOrdinal], from: todayDate)
        
        components.hour = 00
        components.minute = 00
        components.second = 00
        let startDate = calendar.date(from: components)
        
        components.day! += 7 - components.weekdayOrdinal! - 1
        components.hour = 23
        components.minute = 59
        components.second = 59
        let endDate = calendar.date(from: components)
        
        return NSPredicate(format: "date >= %@ AND date =< %@", argumentArray: [startDate!, endDate!])

    }
    
    func getThisMonthDatePredicate() -> NSPredicate {
        let todayDate = Date()
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        var components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: todayDate)
        
        let interval = Calendar.current.dateInterval(of: .month, for: todayDate)!
        let days = Calendar.current.dateComponents([.day], from: interval.start, to: interval.end).day!
        
        components.hour = 00
        components.minute = 00
        components.second = 00
        let startDate = calendar.date(from: components)
        
        components.day = days
        components.hour = 23
        components.minute = 59
        components.second = 59
        let endDate = calendar.date(from: components)
        
        return NSPredicate(format: "date >= %@ AND date =< %@", argumentArray: [startDate!, endDate!])
    }
}
