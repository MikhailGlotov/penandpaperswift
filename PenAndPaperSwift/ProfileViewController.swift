//
//  ProfileViewController.swift
//  PenAndPaperSwift
//
//  Created by Mikhail Glotov on 22/11/16.
//  Copyright © 2016 Mikhail Glotov. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class ProfileViewController: UIViewController, FBSDKLoginButtonDelegate {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImage.image = UIImage.init(named: profileImageName)
        
        profileImage.layer.cornerRadius = profileImage.frame.width / 2
        profileImage.layer.borderWidth = 3.0
        profileImage.layer.borderColor = creamColor.cgColor
        profileImage.clipsToBounds = true
        
        fetchProfileData()
        
        let loginButton = FBSDKLoginButton()
        loginButton.center = self.view.center
        //I'm too lazy today to use constraits :( I'll fix it later in the master I guess
        loginButton.frame.origin.y = 550
        view.addSubview(loginButton)
        
        loginButton.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func fetchProfileData() {
    
    FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email"]).start(completionHandler: { (connection, result, error) -> Void in
        if (error == nil)
        {
            if let data:[String: AnyObject] = result as? [String : AnyObject]
            {
                if let name = data["name"] as? String
                {
                    self.userName.text = name
                }
                else
                {
                    self.userName.text = "John Doe"
                    print("Unable to get user's name")
                }
                
                if let id = data["id"] as? String
                {
                    self.fetchProfileImage(for: id)
                }
                else
                {
                    print("Unable to get user's avatar")
                }
                
            }
            else
            {
                print("Looks like data is corrupted")
            }
        }
        else
        {
            print(error)
        }
        
        })
    }
    
    private func fetchProfileImage(for userId: String) {
        let profilePictureUrl  =  NSURL(string: "https://graph.facebook.com/" + userId + "/picture?type=large")
        let data = try? Data(contentsOf: profilePictureUrl as URL!)
        
        profileImage.contentMode = .scaleAspectFit
        profileImage.image = UIImage(data: data!)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as UIViewController
        show(vc as UIViewController, sender: self)
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            print(error)
            return
        }
    }

}
