//
//  TaskTableViewCell.swift
//  PenAndPaperSwift
//
//  Created by Mikhail Glotov on 17/11/16.
//  Copyright © 2016 Mikhail Glotov. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {
    
    @IBOutlet weak var taskTitle: UILabel!
    @IBOutlet weak var taskDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
