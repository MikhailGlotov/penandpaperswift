//
//  NotificationsManager.swift
//  PenAndPaperSwift
//
//  Created by Mikhail Glotov on 25/11/16.
//  Copyright © 2016 Mikhail Glotov. All rights reserved.
//

import UIKit
import UserNotifications

class NotificationsManager: NSObject {

    func scheduleLocalNotification(title: String, path: URL?, fireDate: Date) {
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents(in: .current, from: fireDate)
        let newComponents = DateComponents(calendar: calendar, timeZone: .current, month: components.month, day: components.day, hour: components.hour, minute: components.minute)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: newComponents, repeats: false)
        
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = "Time to kick some asses!!!"
        content.categoryIdentifier = "myCategory"
        content.sound = UNNotificationSound.default()
        
        let request = UNNotificationRequest.init(identifier: "textNotifications", content: content, trigger: trigger)
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().add(request) {(error) in
            if let error = error {
                print("Uh oh! We had an error: \(error)")
            }
        }

    }
    
    func reScheduleLocalNotification(notification: UNNotification) {
        let content = UNMutableNotificationContent()
        content.title = notification.request.content.title
        content.body = notification.request.content.body
        content.categoryIdentifier = notification.request.content.categoryIdentifier
        content.sound = UNNotificationSound.default()
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: fiveMinutes, repeats: false)
        
        let request = UNNotificationRequest.init(identifier: "textNotifications", content: content, trigger: trigger)
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().add(request) {(error) in
            if let error = error {
                print("Uh oh! We had an error: \(error)")
            }
        }
    }
}

