//
//  FilesManager.swift
//  PenAndPaperSwift
//
//  Created by Mikhail Glotov on 23/11/16.
//  Copyright © 2016 Mikhail Glotov. All rights reserved.
//

import UIKit

class FilesManager: NSObject {

    func loadImage (path: URL ) -> UIImage? {
        let pngImageData = UIImage(contentsOfFile: path.path)
        return pngImageData
    }
    
    func saveImage (image: UIImage, path: URL ) {
        let pngImageData = UIImageJPEGRepresentation(image, 0.5)
        _ = try! pngImageData!.write(to: path)
    }
    
    func getDocumentsFolderUrl() -> URL {
        let documentsFolderUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsFolderUrl
    }
    
    func fileInDocumentsFolder(filename: String) -> URL {
        let fileURL = getDocumentsFolderUrl().appendingPathComponent(filename)
        return fileURL
    }
}
