//
//  NewTaskViewController.swift
//  PenAndPaperSwift
//
//  Created by Mikhail Glotov on 15/11/16.
//  Copyright © 2016 Mikhail Glotov. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

struct ToDoNotification {
    var title: String
    var description: String
    var date: Date
    
    init(title: String, description: String, date: Date) {
        self.title = title
        self.description = description
        self.date = date
    }
}

class NewTaskViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    @IBOutlet weak var pickedImage: UIImageView!
    @IBOutlet weak var taskName: UITextField!
    @IBOutlet weak var taskDescription: UITextField!
    @IBOutlet weak var taskDate: UIDatePicker!
    
    let picker = UIImagePickerController()
    let filesManager = FilesManager()
    let notificationManager = NotificationsManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        
        if !(UIImagePickerController.isSourceTypeAvailable(.camera))
        {
            //hide camera button in case you hardware doesn't have camera
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func addImage(_ sender: UIButton) {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func addNewTask(_ sender: UIButton) {

        let newTask = Task()
        newTask.id = UUID().uuidString
        
        newTask.name = taskName.text!
        newTask.information = taskDescription.text!
        newTask.date = taskDate.date
    
        let realm = try! Realm()
    
        try! realm.write {
            realm.add(newTask)
        }
        
        if ((pickedImage.image) != nil)
        {
            filesManager.saveImage(image: pickedImage.image!, path: filesManager.fileInDocumentsFolder(filename: newTask.id))
            notificationManager.scheduleLocalNotification(title: newTask.name, path: filesManager.fileInDocumentsFolder(filename: newTask.id), fireDate: newTask.date!)
        }
        else
        {
            notificationManager.scheduleLocalNotification(title: newTask.name, path: nil, fireDate: newTask.date!)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        pickedImage.contentMode = .scaleAspectFit
        pickedImage.image = chosenImage
        dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}
