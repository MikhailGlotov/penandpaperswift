//
//  TaskWithDetailsViewController.swift
//  PenAndPaperSwift
//
//  Created by Mikhail Glotov on 24/11/16.
//  Copyright © 2016 Mikhail Glotov. All rights reserved.
//

import UIKit

class TaskWithDetailsViewController: UIViewController {

    @IBOutlet weak var attachedImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    //this one can be refactored into struct for future convinience
    var taskId = String()
    var taskTitle = String()
    var taskDescription = String()
    var taskDate = Date()
    
    let filesManager = FilesManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        attachedImage.image = filesManager.loadImage(path: filesManager.fileInDocumentsFolder(filename: taskId))
        titleLabel.text = taskTitle
        descriptionLabel.text = taskDescription
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
